**Week 4 Progress:**
1. We calculated sprayer calibration and calculation using excel for different pesticides.
2. We have confirm the the type of nozzle to be used ( Y, L or T type).
3. Need to decide how many nozzles we will use and the their placements based on swath overlap.
4. We found out there are 2 options for our payload system components. One of them is ready made, costs RM ~ 2500.
Anther one is DIY uSing parts from Poladrone. Costs around RM ~ 1000.
5. We met Mr Azizi to consult about our project.
