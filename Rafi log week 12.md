**Goals:**
1. Stop the nozzle leaking.
2. Battery endurance test
3. Find CG of whole system (FSI subsystem).
4. Introduce the pump switch control (Software and Control subsystem)
5. Attachment of the spraying system (Boom) to the tank.

**Progress:**

Meeting 1:
1. The leakage was stopped using waterproofing tape.
2. Boomstick clamp was attached to a dummy tank as the same attachment methods will be applied onto the main tank.

Meeting 2:
1. System was assembled including pump, nozzles, boomstick and tubes without any leakage.
2. The assembled system was abled to operate smoothly.

Meeting 3:
1. The tank was filled with 5L of water and the time required to empty it using the pump was recorded.
2. The battery usage for emptying the tank was also recorded.

| Test Run      | Duration      | Battery Usage |
| ------------- |:-------------:| -----:|
| 1             | 1 min 20 sec  | 7%    |
| 2             | 1 min 26 sec  | 6% |


**Things to do:**
1. Pump Switch Control. (Software and Control subsystem)
2. Attach Gondola to tank.
3. Attach whole payload to airship.
