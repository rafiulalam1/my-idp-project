**Goals:**

1. Pump Switch Control. (Software and Control subsystem)
2. Attach Gondola to tank.
3. Attach whole payload to airship.

**Progress:**

Meeting 1:
1. It was suggested to add a plate and truss system to support the gondola.
2. The gondola will be attached to the plate using velcro.

Meeting 2:
1. The material choice for the plate was plywood.
2. An L bracket was added to support the gondola. The bracket was screwed into the tank.
3. The plywood plate was attached to the tank using expoxy and double sided tape.
4. Velcro strap was attached to the plate for attachment with the gondola.

**Things to do:**
1. Flight Test
2. Pump Switch Control. (Software and Control subsystem)
