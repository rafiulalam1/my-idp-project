**What went wrong?**

A. Technical issues:
1. Issues with control system configuration.
2. Srong wind.
3. Attachment method of thruster arms to the airship.
4. Inadequate testing.

B. Project Management issues:
1. Delay of shipments of project resources.

**What went right?**

1. Scope of the project was well defined.
2. Schedule of the project was realistic.
3. project team meetings were fruitful and solutions to several problems were found.
4. subsytem goals were achieved on a weekly basis.
