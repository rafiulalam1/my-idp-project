**Goals:**

1. Flight Test
2. Pump Switch Control. (Software and Control subsystem)

**Progress:**

Meeting 1:
1. The pump was controlled via a relay switch and wifi module.
2. An app was created to turn the pump on and off.
3. The pump was tuned on anf off using the wifi module and arduin coding via the app.
4. Details of this system can be found on Software and Control subsystem's repository.

Meeting 2:
1. Pre-Flight checklist for the payload subsystem was followed and the system ran smoothly.
2. Due to some limitations, system attachment to airship was not completed. (discussed in failure report)

**Things to do:**

Post mortem and failure report
