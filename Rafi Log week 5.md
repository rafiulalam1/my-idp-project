**Week 5 Progress:**
1. Students were added to the gitlab payload repositery.
2. We discussed about the sprayer design of the payload sytem. 
3. We finalised what components we should get for our payload system. 
4. We decided the size of the pesticide tank to be 5.5L.
5. The sprayer pipes will have T and L type nozzle mounted.
6. We are yet calculate the swath overlap based on different nozzle placement and altitude.
Our aim is have more than 50% swath overlap.
