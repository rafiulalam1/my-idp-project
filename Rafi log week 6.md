**Week 6 Progress:**
1. Discussed with Mr. Fiqri and Design team for design improvements.
2. We selected Lambda-cyhalothrin and Sharda lambda cyhalothrin as our choice of pesticide.
3. We calculated the Gallons per minute (GPM) for various speed for fixed nozzle spacing and Gallon per Acre (GPA).
4. We calculated the boom height for 50% and 100% overlap for various nozzle fan angles and forward angles.We assumed pump flowrate of 1.13GPM based on calculations on pesticide mixing 217.8 GPA, field speed assumption (2.2MPH) and nozzle spacing assumption(13.78inches).
5. We ran into some trouble regarding the nozzle height for good crop coverage but were told to focus on making the concept work where additional changes could be made over time.
6. We disgned the strcuture of our payload.
7. 3D drawing of the structure was created.
8. Most Materials and components required for the structure was selected.
9. Some materials were bought while some has to be 3D printed or ordered from online.
 
