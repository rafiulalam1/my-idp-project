**Goals:**
1. Resolve the issue with pump and nozzle.
2. Assemble pump and nozzle do a test run of the spraying system.

**Progress:**
1. Lab was unaccessible as the lectures/labs were postponed due to the flood and covid outbreak in the lab.
2. The pump issue was resolved as new pump was acquired according to required specifications from Paladrone. 

**Things to do:**
1. Assemble pump and nozzle do a test run of the spraying system.
