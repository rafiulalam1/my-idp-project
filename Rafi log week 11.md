**Goals:**
1. Assemble pump and nozzle do a test run of the spraying system.

**Progress:**

3 meetings were done to achieve the goals.

_Meeting 1:_
1. Nozzles and flextube are assembled on pvc pipe for first test run and 2 nozzles were tested.
2. The functionality of the pump and nozzles were tested on different altitute (ground level and elevated).
3. The system was attached to the roof to mimic the airship height.
4. The pump and nozzles functioned well.

_Meeting 2:_
1. Nozzles and flextube were assembled on 1.29m long carbon fibre rods with 43cm nozzle spacing.
2. Adding 'T' connector to connect 4 nozzles together.
3. The functionality of the whole system was tested at ground level.
4. It was observed that the nozzles leaked at a few parts.
5. 'T' connector and pump functioned well.

_Meeting 3:_
1. Separate tank is used to test different types of attachment styles (screws, clamps, nails etc).
2. It was observed that expanding wall plugs and screws can withstand the pump well.
3. It was also observed that the second tank which is thinner than orignal tank can hold the pump well without straining or cracking.

**Things to do:**
1. Stop the nozzle leaking.
2. Battery endurance test
3. Find CG of whole system (FSI subsystem).
4. Introduce the pump switch control (Software and Control subsystem)
5. Attachment of the spraying system (Boom) to the tank.
