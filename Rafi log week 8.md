**Goals:**
1. Design a new mounting system considering the battery and power board accessibility, CG location, weight, thurster arm location.
2. Test pump and nozzle.
3. Test flex tube and boom stick.

**Progress:**
1. New mounting system was developed and updated into the tracker.
2. Two alternative designs were created.
3. The estimated weight and accessibility of each design was calculated and compared. The results were updated to the tracker.
4. The best attachemnt system was chosen.

**Things to do:**
1. There was a issue with pump and nozzle. Need to resolve it.
2. Assemble pump and nozzle do a test run of the spraying system.

